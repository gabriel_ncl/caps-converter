﻿using Caps_Converter.Controller;

namespace Caps_Converter
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertingController controller = new ConvertingController();
            controller.execution();
        }
    }
}
