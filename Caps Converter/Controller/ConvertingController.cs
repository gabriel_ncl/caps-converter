﻿using Caps_Converter.Model;
using Caps_Converter.View;
using System;

namespace Caps_Converter.Controller
{
    class ConvertingController
    {
        public IConvertingView view;
        public Convertable convertable;
        private static int MAX_INPUT_STRING = 8;

        public void execution()
        {
            while (keepRunning()) {
                view = new ConsoleView();
                view.renderMenu(ref convertable);
                if (convertable.input.Length > MAX_INPUT_STRING)
                {
                    view.inputSizeError();
                }
                else
                {
                    convert(convertable.input);
                    view.renderOutput(ref convertable);
                }
            }
        }

        public void quit()
        {
            Environment.Exit(0);
        }

        public bool keepRunning()
        {
            Console.WriteLine("Continuer ?");
            string key = Console.ReadLine();
            if (key == "n")
            {
                return false;
            }
            
            if (key == "y")
            {
                return true;
            }
            else
            {
                return keepRunning();
            }
        }

        public string convert(String input)
        {
            input = convertable.input;
            convertable.output = input.ToUpper();
            string output = convertable.output;
            return output;
        }
    }
}