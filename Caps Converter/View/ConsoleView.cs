﻿using System;
using Caps_Converter.Model;

namespace Caps_Converter.View
{
    class ConsoleView : IConvertingView
    {
        public ConsoleView()
        {

        }
        public void renderMenu(ref Convertable convertable)
        {
            Console.WriteLine("Ecrivez la chaîne de caractères : ");
            convertable = new Convertable();
            convertable.input = Console.ReadLine();
        }
        public void renderOutput(ref Convertable convertable)
        {
            Console.WriteLine("Transformation : " + convertable.output);
        }

        public void inputSizeError()
        {
            Console.WriteLine("Erreur pas plus de 8 lettres");
        }

        public void exitOrNot()
        {
            Console.WriteLine("Voulez vous quitter Y/N (Yes / No)");
        }
    }
}