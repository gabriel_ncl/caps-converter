﻿using Caps_Converter.Model;

namespace Caps_Converter.View
{
    interface IConvertingView
    {
        public void renderMenu(ref Convertable convertable)
        {

        }
        public void renderOutput(ref Convertable convertable)
        {

        }

        public void inputSizeError()
        {

        }
    }
}